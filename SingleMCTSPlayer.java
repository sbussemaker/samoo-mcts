package tracks.singlePlayer.stefan.samooMCTS;

import java.util.Random;

import core.game.StateObservation;
import tools.ElapsedCpuTimer;
import tools.Pair;
import tracks.singlePlayer.stefan.samooMCTS.ParameterTuning.MABManager;
import tracks.singlePlayer.stefan.samooMCTS.ParameterTuning.ParameterSetting;
import tracks.singlePlayer.stefan.samooMCTS.options.OptionsHelper;
import tracks.singlePlayer.tools.Logging.Introspection;

/**
 * Created with IntelliJ IDEA.
 * User: Diego
 * Date: 07/11/13
 * Time: 17:13
 */
public class SingleMCTSPlayer
{
    /**
     * Root of the tree.
     */
    public SingleTreeNode m_root;

    /**
     * Random generator.
     */
    public Random m_rnd;

    public MABManager parameterManager = new MABManager();
    public Introspection introspection;

    // SelfAdapting MCTS
    public static int ROLLOUT_DEPTH;
    public static double K;

    public SingleMCTSPlayer(Random a_rnd)
    {
        m_rnd = a_rnd;
    }

    /**
     * Inits the tree with the new observation state in the root.
     * @param a_gameState current state of the game.
     */
    public void init(StateObservation a_gameState)
    {
        ParameterSetting settings = Agent.ENABLE_SELF_ADAPTIVE ? parameterManager.paramsChooseParamValues()
                                                               : new ParameterSetting();

        ROLLOUT_DEPTH = settings.ROLLOUT_DEPTH;
        K             = settings.K;

        m_root = new SingleTreeNode(m_rnd);
        m_root.state = a_gameState;        
        m_root.optionsHelper = new OptionsHelper(a_gameState, Agent.num_actions);
    }

    /**
     * Runs MCTS to decide the action to take. It does not reset the tree.
     * @param elapsedTimer Timer when the action returned is due.
     * @return the action to execute in the game.
     */
    public int run(ElapsedCpuTimer elapsedTimer)
    {
        //Do the search within the available time.
        m_root.mctsSearch(elapsedTimer);
        
        //Determine the best action to take and return it.
        Pair<Integer, Double> results = (Agent.ENABLE_MULTI_OBJECT) ? m_root.recommendationPolicy()
                                                                    : m_root.bestAction();

        int action = results.getKey();
        double reward = results.getValue();

        introspection = m_root.introspect();
        introspection.setRecommendedAction(Agent.actions[action]);
        introspection.computeConsumedBudget(elapsedTimer);
        introspection.computeConvergence();

        if (Agent.ENABLE_SELF_ADAPTIVE)
            parameterManager.updateValuesStats(reward);

        return action;
    }

}
