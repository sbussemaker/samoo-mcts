# SAMOO-MCTS
This repository contains the code written for my Master's project. The title of the project is: _Self-adaptive Multi-objective Option Monte-Carlo Tree Search for General Video Game Play_. The thesis is available here: [http://fse.studenttheses.ub.rug.nl/id/eprint/19553](http://fse.studenttheses.ub.rug.nl/id/eprint/19553).

## The GVGAI framework
The code is written to be used with the General Video Game Competition. The competition code is therefore required to be able to run the code in this repo. More information on the competition and where to download the framework can be found [http://www.gvgai.net/](http://www.gvgai.net/).
