package tracks.singlePlayer.stefan.samooMCTS.pheromones;

public class BlockPosition {
    
    public final int first;
    public final int second;
    
    public BlockPosition(int first, int second){
        this.first = first;
        this.second = second;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + first;
        result = prime * result + second;
        return result;
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject){
            return true;
        }
        
        if (!(otherObject instanceof BlockPosition)){
            return false;
        }
        
        BlockPosition other = (BlockPosition) otherObject;
        return (first == other.first  &&
                second == other.second);
    }
    
    @Override
    public String toString(){
        return "(" + first + ", " + second + ")";
    }

}
