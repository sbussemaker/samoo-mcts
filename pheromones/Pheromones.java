package tracks.singlePlayer.stefan.samooMCTS.pheromones;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import core.game.StateObservation;
import tools.Vector2d;

/**
 * Pheromone map [Multi-Objective Tree Search Approaches for General Video Game
 * Playing, 2016] Each cell contains a pheromone value p_{i,j} in [0, 1] where i
 * and j are coordinates pheromones are expelled by avatar and spread to nearby
 * cells p_{i,j} decays with time
 * 
 * Game state information -> StateObservation
 * https://github.com/GAIGResearch/GVGAI/wiki/Querying-the-state-of-the-game
 * 
 * Individual objects information -> Observation
 * https://github.com/GAIGResearch/GVGAI/wiki/Information-about-other-sprites-in-the-game
 */
public class Pheromones {
    private final double decay     = .99;
    private final double diffusion = .4;
    private final double excretion = .9;           // Not present in literature, experiment

    private final int mapHeightPixels;
    private final int mapWidthPixels;
    private final int pixelsPerBlock;
    private final int mapHeightBlocks;
    private final int mapWidthBlocks;

    private double[][] pheromoneMap;

    public Pheromones(StateObservation state) {
        Dimension pixels = state.getWorldDimension();
        this.pixelsPerBlock = state.getBlockSize();
        this.mapHeightPixels = pixels.height;
        this.mapWidthPixels = pixels.width;
        this.mapHeightBlocks = (mapHeightPixels / pixelsPerBlock);
        this.mapWidthBlocks = (mapWidthPixels / pixelsPerBlock);
        
        pheromoneMap = new double[mapWidthBlocks][mapHeightBlocks];
    }

    public void update(StateObservation state) {
        BlockPosition avatarBlock = positionToBlockPosition(state.getAvatarPosition());
        if (isValidBlock(avatarBlock)) {
            pheromoneMap[avatarBlock.first][avatarBlock.second] += excretion;
        }
        
        double[][] newMap = new double[mapWidthBlocks][mapHeightBlocks];
        for (int x = 0; x < pheromoneMap.length; x++) {
            for (int y = 0; y < pheromoneMap[x].length; y++) {
                double sumTrail = neighbourSpread(x, y);
                newMap[x][y] = diffusion * sumTrail + (1 - diffusion) * decay * pheromoneMap[x][y];
                // double val = newMap[x][y];
                // if ((val < 0) || (val > 1)) {
                //     System.out.println("Out of bounds Pheromones!" + val);
                // }
            }
        }

        pheromoneMap = newMap;
    }

    private boolean isValidBlock(BlockPosition block) {
        return block.first >= 0             && 
               block.second >= 0            &&
               block.first < mapWidthBlocks && 
               block.second < mapHeightBlocks;
    }

    private double neighbourSpread(int x, int y) {
        // sum of pheromone trail in all neighbouring cells divided by the number of neighbouring cells
        double sum = 0;
        int numNeighbours = 0;

        BlockPosition left  = new BlockPosition(x - 1, y);
        BlockPosition up    = new BlockPosition(x, y + 1);
        BlockPosition right = new BlockPosition(x + 1, y);
        BlockPosition down  = new BlockPosition(x, y - 1);

        if (isValidBlock(left)) {
            sum += getPheromoneStrength(left);
            numNeighbours++;
        }
        if (isValidBlock(up)) {
            sum += getPheromoneStrength(up);
            numNeighbours++;
        }
        if (isValidBlock(right)) {
            sum += getPheromoneStrength(right);
            numNeighbours++;
        }
        if (isValidBlock(down)) {
            sum += getPheromoneStrength(down);
            numNeighbours++;
        }

        return sum / numNeighbours;
    }

    public double getPheromoneStrength(Vector2d position) {
        BlockPosition pos = positionToBlockPosition(position);
        // System.out.println(pos);
        return isValidBlock(pos) ? pheromoneMap[pos.first][pos.second] : 0;
    }

    public double getPheromoneStrength(BlockPosition block) {
        return isValidBlock(block) ? pheromoneMap[block.first][block.second] : 0;
    }

    private BlockPosition positionToBlockPosition(Vector2d position) {
        int x = ((int) position.x) / pixelsPerBlock;
        int y = ((int) position.y) / pixelsPerBlock;

        return new BlockPosition(x, y);
    }

    public void draw(Graphics2D g) {
        
        final java.awt.Color yellow = java.awt.Color.YELLOW;
        final java.awt.Color red = java.awt.Color.RED;
        final java.awt.Color green = java.awt.Color.GREEN;

        BufferedImage imgBuf = new BufferedImage(mapWidthPixels, mapHeightPixels, BufferedImage.TYPE_INT_ARGB);
        Graphics2D img = imgBuf.createGraphics();
        
        // draw pheromones
        for (int x = 0; x < mapWidthBlocks; ++x) {
            for (int y = 0; y < mapHeightBlocks; ++y) {
                double pheromone = pheromoneMap[x][y];
                Vector2d pos = cellToPosition(x,  y);
                
                int colorR = 0;
                int colorG = 0;
                int colorB = 0;
                
                if (pheromone == 0.5) {
                    colorB = yellow.getBlue();
                    colorG = yellow.getGreen();
                    colorR = yellow.getRed();
                } else if (pheromone > 0.5) {
                    pheromone = pheromone > 1.0 ? 1.0 : pheromone;
                    double redRatio = pheromone;
                    double yellowRatio = 1.0 - redRatio;
                    
                    colorB = (int)(redRatio * red.getBlue() + yellowRatio * yellow.getBlue());
                    colorG = (int)(redRatio * red.getGreen() + yellowRatio * yellow.getGreen());
                    colorR = (int)(redRatio * red.getRed() + yellowRatio * yellow.getRed());
                } else {
                    pheromone = pheromone < 0.0 ? 0.0 : pheromone;
                    double greenRatio = 1.0 - pheromone;
                    double yellowRatio = 1.0 - greenRatio;
                    
                    colorB = (int)(greenRatio * green.getBlue() + yellowRatio * yellow.getBlue());
                    colorG = (int)(greenRatio * green.getGreen() + yellowRatio * yellow.getGreen());
                    colorR = (int)(greenRatio * green.getRed() + yellowRatio * yellow.getRed());
                }
                
                // System.out.println(colorR + " " + colorG + " " + colorB);
                Color color = new Color(colorR, colorG, colorB, 100);
                img.setColor(color);
                img.fillRect((int) (pos.x), (int) (pos.y), pixelsPerBlock, pixelsPerBlock);
            }
        }

        g.drawImage(imgBuf, 0, 0, mapWidthPixels, mapHeightPixels, null);
    }

    private Vector2d cellToPosition(int x, int y) {
		return new Vector2d(x * pixelsPerBlock, y * pixelsPerBlock);
	}
}
