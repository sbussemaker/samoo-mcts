package tracks.singlePlayer.stefan.samooMCTS;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import core.game.Observation;
import core.game.StateObservation;
import core.player.AbstractPlayer;
import ontology.Types;
import tools.ElapsedCpuTimer;
import tools.Vector2d;
import tools.pathfinder.Node;
import tools.pathfinder.PathFinder;
import tracks.singlePlayer.stefan.samooMCTS.heuristics.Exploration;
import tracks.singlePlayer.stefan.samooMCTS.heuristics.Gamescore;
import tracks.singlePlayer.stefan.samooMCTS.heuristics.WeightedHeuristic;
import tracks.singlePlayer.stefan.samooMCTS.pheromones.Pheromones;

/**
 * Created with IntelliJ IDEA. User: ssamot Date: 14/11/13 Time: 21:45 This is
 * an implementation of MCTS UCT
 * 
 * uses UCT in tree policy select node with max visits as recommendation policy,
 * in case of tie: node w/ highest average reward
 */
public class Agent extends AbstractPlayer {

    public static final boolean ENABLE_SELF_ADAPTIVE = true;
    public static final boolean ENABLE_MULTI_OBJECT = true;
    public static final boolean ENABLE_OPTIONS = true;

    public static final boolean DEBUG_DRAW_PHEREMONES = false;
    public static final boolean DEBUG_OPTIONS_DRAW_PATH = false;
    public static PathPlanner pathPlanner;

    public static int num_actions;
    public static Types.ACTIONS[] actions;

    public static WeightedHeuristic[] objectives;
    public static int NUM_OBJECTIVES;

    public static Pheromones pheromones;

    protected SingleMCTSPlayer mctsPlayer;

    /**
     * Public constructor with state observation and time due.
     * 
     * @param so           state observation of the current game.
     * @param elapsedTimer Timer for the controller creation.
     */
    public Agent(StateObservation so, ElapsedCpuTimer elapsedTimer) {
        // Get the actions in a static array.
        ArrayList<Types.ACTIONS> act = so.getAvailableActions();
        actions = new Types.ACTIONS[act.size()];
        for (int i = 0; i < actions.length; ++i) {
            actions[i] = act.get(i);
        }
        num_actions = actions.length;

        if (ENABLE_MULTI_OBJECT) {
            pheromones = new Pheromones(so);
        }

        if (ENABLE_OPTIONS) {
            pathPlanner = new PathPlanner(so);
        }

        objectives = ENABLE_MULTI_OBJECT
                ? new WeightedHeuristic[] { new WeightedHeuristic(new Gamescore(), 1),
                        new WeightedHeuristic(new Exploration(), 1), }
                : new WeightedHeuristic[] { new WeightedHeuristic(new Gamescore(), 1), };

        NUM_OBJECTIVES = objectives.length;

        // Create the player.

        mctsPlayer = getPlayer(so, elapsedTimer);

        System.out.println(
                "Creating agent with parameter tuning \t" + new String(ENABLE_SELF_ADAPTIVE ? "enabled" : "disabled"));
        System.out.println(
                "Creating agent with multiple objects \t" + new String(ENABLE_MULTI_OBJECT ? "enabled" : "disabled"));
        System.out.println("Creating agent with options \t\t" + new String(ENABLE_OPTIONS ? "enabled" : "disabled"));
    }

    public SingleMCTSPlayer getPlayer(StateObservation so, ElapsedCpuTimer elapsedTimer) {
        return new SingleMCTSPlayer(new Random());
    }

    public static ArrayList<Node> getPathTo(StateObservation so, Observation goal) {
        return pathPlanner.getPathTo(so, goal);
    }

    /**
     * Picks an action. This function is called every game step to request an action
     * from the player.
     * 
     * @param stateObs     Observation of the current state.
     * @param elapsedTimer Timer when the action returned is due.
     * @return An action for the current state
     */
    public Types.ACTIONS act(StateObservation stateObs, ElapsedCpuTimer elapsedTimer) {

        // Update pheromones
        if (ENABLE_MULTI_OBJECT)
            pheromones.update(stateObs);

        // Set the state observation object as the new root of the tree.
        mctsPlayer.init(stateObs);

        // Determine the action using MCTS...
        int action = mctsPlayer.run(elapsedTimer);

        logAllActions(actions);
        logIntrospection(mctsPlayer.introspection);

        // ... and return it.
        return actions[action];
    }

    @Override
    public void draw(Graphics2D g) {
        if (ENABLE_MULTI_OBJECT && DEBUG_DRAW_PHEREMONES) {
            pheromones.draw(g);
        }

        if (ENABLE_OPTIONS && DEBUG_OPTIONS_DRAW_PATH) {
            pathPlanner.draw(g);
        }
    }

    @Override
    public void result(StateObservation stateObservation, ElapsedCpuTimer elapsedCpuTimer) {
        // Include your code here to know how it all ended.
        if (ENABLE_SELF_ADAPTIVE)
            mctsPlayer.parameterManager.logStatistics();
    }
}
