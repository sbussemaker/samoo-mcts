package tracks.singlePlayer.stefan.samooMCTS.ParameterTuning;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import tools.Pair;
import tools.Utils;

/**
 *
 */
public class MABGlobal {
    private int currentArmIdx;
    private int totalVisits;
    private List<Pair<ParameterSetting, BanditArm>> items = new ArrayList<>(0);

    private double[] bounds = new double[]{Double.MAX_VALUE, -Double.MAX_VALUE};
    private Random rnd = new Random();
    private double epsilon = 1e-6;
    private double UCB1_K = 0.7;

    // Chose to make MABs stateful, so I don't have to pass parameter settings around
    public void updateArmStats(double reward) {
        Pair<ParameterSetting, BanditArm> current = items.get(currentArmIdx);
        BanditArm arm = current.getValue();
        arm.totalScore += reward;
        arm.n_visits += 1;
        totalVisits += 1;
        
        if(reward < bounds[0])
            bounds[0] = reward;
        if(reward > bounds[1])
            bounds[1] = reward;
    }

    public void add(ParameterSetting settings) {
        boolean contains = false;
        for (Pair<ParameterSetting, BanditArm> arm : items)
            if (arm.getKey().equals(settings))
                contains = true;
        
        if (!contains)
            items.add(new Pair<ParameterSetting, BanditArm>(settings, new BanditArm()));

    }

    public void setCurrentArm(ParameterSetting settings) {
        // apparently, explore was chosen, therefore, set correct currentArmIdx
        for (int idx = 0; idx < items.size(); idx++) {
            Pair<ParameterSetting, BanditArm> arm = items.get(idx);
            if (arm.getKey().equals(settings))
                currentArmIdx = idx;
        }
    }

    public int size() {
        return items.size();
    }

    public ParameterSetting chooseCombinations() {
        ParameterSetting selected = null;
        double bestValue = -Double.MAX_VALUE;
        int selectedIdx = -1;
        for (int idx = 0; idx < items.size(); idx++)
        {
            Pair<ParameterSetting, BanditArm> pair = items.get(idx);
            BanditArm arm = pair.getValue();
            double hvVal = arm.totalScore;
            double denom = arm.n_visits;
            double childValue =  hvVal / (denom + this.epsilon);

            childValue = Utils.normalise(childValue, bounds[0], bounds[1]);

            double uctValue = childValue +
                              UCB1_K * Math.sqrt(Math.log(totalVisits + 1) / (denom + this.epsilon));

            uctValue = Utils.noise(uctValue, this.epsilon, rnd.nextDouble());     //break ties randomly

            // small sampleRandom numbers: break ties in unexpanded nodes
            if (uctValue > bestValue) {
                selected = pair.getKey();
                selectedIdx = idx;
                bestValue = uctValue;
            }
        }
        if (selected == null)
        {
            throw new RuntimeException("Warning! returning null");
        }

        currentArmIdx = selectedIdx;
        return selected;
    }

    public void logStatistics() {
        for (Pair<ParameterSetting, BanditArm> item : items)
        {
            ParameterSetting setting = item.getKey();
            BanditArm arm = item.getValue();

            System.out.println("[" + setting.K + ", " + setting.ROLLOUT_DEPTH + "]\t"
                               + arm.n_visits + " visits, " + arm.totalScore + " totalScore");
        }
    }
}
