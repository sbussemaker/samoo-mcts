package tracks.singlePlayer.stefan.samooMCTS.ParameterTuning;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 *
 */
public class MABManager {
    private MABGlobal mab_g = new MABGlobal();
    private MABLocal<Integer> mab_rd = new MABLocal<Integer>(new ArrayList<>(Arrays.asList(
        1, 5, 10, 20, 50, 70
        )));
    private MABLocal<Double> mab_k = new MABLocal<Double>(new ArrayList<>(Arrays.asList(
        0.8, 1.0, 1.4, 2., 2.4
        )));
    private double EPSILON_0 = 0.75;

    private Random rnd = new Random();

    public ParameterSetting paramsChooseParamValues() {
        ParameterSetting settings = new ParameterSetting();

        double rndD = rnd.nextDouble();
        boolean explore = rndD < EPSILON_0;
        if (explore || mab_g.size() == 0) {         // exploration
            settings.ROLLOUT_DEPTH = mab_rd.chooseValue();
            settings.K             = mab_k.chooseValue();
            mab_g.add(settings);

            mab_g.setCurrentArm(settings);
        } else {                                    // exploitation
            settings = mab_g.chooseCombinations();
            
            mab_rd.setCurrentArm(settings.ROLLOUT_DEPTH);
            mab_k.setCurrentArm(settings.K);
        }

        return settings;
    }

    // Chose to make MABs stateful, so I don't have to pass parameter settings around
    public void updateValuesStats(double reward) {
        mab_g.updateArmStats(reward);

        mab_rd.updateArmStats(reward);
        mab_k.updateArmStats(reward);
    }

    /** Writes parameter visits to sytem print */
    public void logStatistics() {
        System.out.println("Global MAB:");
        mab_g.logStatistics();
        System.out.println("Local MAB for Rollout depth:");
        mab_rd.logStatistics();
        System.out.println("Local MAB for K:");
        mab_k.logStatistics();
    }
}
