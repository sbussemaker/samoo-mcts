package tracks.singlePlayer.stefan.samooMCTS.ParameterTuning;

public class ParameterSetting {
    public int ROLLOUT_DEPTH = 10;
    public double K = Math.sqrt(2);

    @Override
    public int hashCode() {
        final int prime = 37;
        int result = 1;
        result = prime * result + ROLLOUT_DEPTH;
        // https://stackoverflow.com/a/9650824
        result = prime * result + Double.valueOf(K).hashCode();
        return ROLLOUT_DEPTH;
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject){
            return true;
        }
        
        if (!(otherObject instanceof ParameterSetting)){
            return false;
        }
        
        ParameterSetting other = (ParameterSetting) otherObject;
        return (ROLLOUT_DEPTH == other.ROLLOUT_DEPTH  &&
                K == other.K);
    }
}
