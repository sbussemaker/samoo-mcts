package tracks.singlePlayer.stefan.samooMCTS.ParameterTuning;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import tools.Pair;
import tools.Utils;

/**
 *
 */
public class MABLocal<T> {
    private int currentArmIdx;
    private int totalVisits;
    private List<Pair<T, BanditArm>> items = new ArrayList<>(0);

    private double[] bounds = new double[]{Double.MAX_VALUE, -Double.MAX_VALUE};
    private Random rnd = new Random();
    private double epsilon = 1e-6;
    private double UCB1_rb = 0.7;

    public MABLocal(List<T> items) {
        for (T item : items)
            this.items.add(new Pair<T, BanditArm>(item, new BanditArm()));
    }

    // arms, init with possible values
    // each arm will contain value, number of visits and total score

    public void updateArmStats(double reward) {
        Pair<T, BanditArm> item = items.get(currentArmIdx);
        BanditArm arm = item.getValue();
        arm.totalScore += reward;
        arm.n_visits += 1;
        totalVisits += 1;
        
        if(reward < bounds[0])
            bounds[0] = reward;
        if(reward > bounds[1])
            bounds[1] = reward;
    }

    public void setCurrentArm(T value) {
        // apparently, exploit was chosen, therefore, set correct currentArmIdx
        for (int idx = 0; idx < items.size(); idx++) {
            Pair<T, BanditArm> arm = items.get(idx);
            if (arm.getKey().equals(value))
                currentArmIdx = idx;
        }
    }

    // public T get(int idx) {
    //     return items.get(idx);
    // }

    public T chooseValue() {
        // UCB1
        T selected = null;
        double bestValue = -Double.MAX_VALUE;
        int selectedIdx = -1;
        for (int idx = 0; idx < items.size(); idx++)
        {
            Pair<T, BanditArm> pair = items.get(idx);
            BanditArm arm = pair.getValue();

            double hvVal;
            int denom;
            if (arm.n_visits == 0) { // hack to make sure all options are explored, not nice
                hvVal = Double.MAX_VALUE;
                denom = 1;
            } else {
                hvVal = arm.totalScore;
                denom = arm.n_visits;
            }
            
            double childValue =  hvVal / (denom + this.epsilon);

            childValue = Utils.normalise(childValue, bounds[0], bounds[1]);

            double uctValue = childValue +
                              UCB1_rb * Math.sqrt(Math.log(totalVisits + 1) / (denom + this.epsilon));

            uctValue = Utils.noise(uctValue, this.epsilon, rnd.nextDouble());     //break ties randomly

            // small sampleRandom numbers: break ties in unexpanded nodes
            if (uctValue > bestValue) {
                selected = pair.getKey();
                selectedIdx = idx;
                bestValue = uctValue;
            }
        }
        if (selected == null)
        {
            throw new RuntimeException("Warning! returning null");
        }

        currentArmIdx = selectedIdx;
        return selected;
    }

    public void logStatistics() {
        for (Pair<T, BanditArm> item : items)
        {
            T setting = item.getKey();
            BanditArm arm = item.getValue();

            System.out.println("[" + setting + "]\t"
                               + arm.n_visits + " visits, " + arm.totalScore + " totalScore");
        }
    }
}
