package tracks.singlePlayer.stefan.samooMCTS;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import core.game.StateObservation;
import tools.ElapsedCpuTimer;
import tools.Pair;
import tools.Utils;
import tracks.singlePlayer.stefan.samooMCTS.options.Option;
import tracks.singlePlayer.stefan.samooMCTS.options.OptionsHelper;
import tracks.singlePlayer.stefan.samooMCTS.pareto.ParetoFront;
import tracks.singlePlayer.stefan.samooMCTS.pareto.Solution;
import tracks.singlePlayer.tools.Logging.Introspection;

public class SingleTreeNode
{
    public double epsilon = 1e-6;
    public StateObservation state;
    public SingleTreeNode parent;
    public SingleTreeNode[] children;
    public double totValue;
    public int nVisits;
    public Random m_rnd;
    public int m_depth;
    
    public StateObservation rootState;
    
    public int bestAction = 0;
    public double bestValue = -1;
    
    // Options MCTS
    public OptionsHelper optionsHelper;

    // Multi-Objective MCTS
    public ParetoFront paretoSet = new ParetoFront();
    public double[] accumulatedRewardVector = new double[Agent.NUM_OBJECTIVES];
    protected double[][] bounds;

    // Introspective MCTS
    private Introspection introspection;

    public SingleTreeNode(Random rnd) {
        this(null, null, rnd);
    }

    public SingleTreeNode(StateObservation state, SingleTreeNode parent, Random rnd) {
        this.state = state;
        this.parent = parent;
        this.m_rnd = rnd;

        bounds = new double[Agent.NUM_OBJECTIVES][2];
        for (int idx = 0; idx < Agent.NUM_OBJECTIVES; idx++)
            bounds[idx] = new double[] { Double.MAX_VALUE, -Double.MAX_VALUE };

        children = new SingleTreeNode[Agent.num_actions];
        introspection = new Introspection(Agent.num_actions);
        totValue = 0.0;
        if(parent != null)
            m_depth = parent.m_depth+1;
        else
            m_depth = 0;

        if (state != null)
            optionsHelper = new OptionsHelper(state, Agent.num_actions);
    }


    public void mctsSearch(ElapsedCpuTimer elapsedTimer) {

        double avgTimeTaken = 0;
        double acumTimeTaken = 0;
        long remaining = elapsedTimer.remainingTimeMillis();
        int numIters = 0;

        int remainingLimit = 5;
        while(remaining > 2 * avgTimeTaken && remaining > remainingLimit){
            ElapsedCpuTimer elapsedTimerIteration = new ElapsedCpuTimer();
            SingleTreeNode selected = treePolicy();
            Solution delta = selected.rollOut();
            backUp(selected, delta);

            numIters++;
            acumTimeTaken += (elapsedTimerIteration.elapsedMillis()) ;
            //System.out.println(elapsedTimerIteration.elapsedMillis() + " --> " + acumTimeTaken + " (" + remaining + ")");
            avgTimeTaken  = acumTimeTaken/numIters;

            for (int i = 0; i < Agent.num_actions; i++) {
                // TODO: Child value is no longer determined with nVisits
                // Instead, it depends on whether multi-objective was used
                // Without, use bestAction()
                // With,    use recommendationPolicy()
                if (children[i] != null && children[i].nVisits > bestValue) {
                    bestAction = i;
                    bestValue = children[i].nVisits;
                    introspection.setBestActionFoundAt(acumTimeTaken);
                }
            }

            remaining = elapsedTimer.remainingTimeMillis();
        }
        
        // System.out.println("Nvisits: " + nVisits);
        for (int child_idx = 0; child_idx < children.length; child_idx++) {
            if (children[child_idx] == null) { continue; }
            introspection.setProbCon(child_idx, children[child_idx].nVisits / (double)nVisits);
            introspection.setActionValue(child_idx, children[child_idx].value());
        }
    }

    public SingleTreeNode treePolicy() {

        SingleTreeNode cur = this;

        while (!cur.state.isGameOver() && cur.m_depth < SingleMCTSPlayer.ROLLOUT_DEPTH)
        {
            if (Agent.ENABLE_OPTIONS) {
                // if option stopts in state s
                if (cur.optionsHelper.currentOption != null && cur.optionsHelper.optionStops(cur.state)) 
                    return cur;
    
                Option[] p_s;
                if (cur.optionsHelper.currentOption == null) {
                    // p_s = available options
                    p_s = cur.optionsHelper.availableOptions();
                } else {
                    // no new option can be selected
                    p_s = new Option[]{ cur.optionsHelper.currentOption };
                }
    
                // set m to expanded options
                Option[] m = cur.optionsHelper.expandedOptions();
    
                // if all options are expanded
                if (cur.optionsHelper.equals(p_s, m)) {
                    SingleTreeNode next = cur.uct();        // select child node
                    cur = next;                             // continue loop with new node s'
                } else {
                    Option w = p_s.length > 1 ? cur.optionsHelper.randomElement(p_s, m) : p_s[0];
                    cur.optionsHelper.currentOption = w;
                    int action = cur.optionsHelper.getAction(w, cur.state);
                    SingleTreeNode next = cur.expand(action);
                    next.optionsHelper.currentOption = cur.optionsHelper.currentOption;
                    cur = next;
                }
            } else {
                if (cur.notFullyExpanded()) {
                    int bestAction = 0;
                    double bestValue = -1;

                    for (int i = 0; i < children.length; i++) {
                        double x = m_rnd.nextDouble();
                        if (x > bestValue && children[i] == null) {
                            bestAction = i;
                            bestValue = x;
                        }
                    }

                    return cur.expand(bestAction);
    
                } else {
                    SingleTreeNode next = cur.uct();
                    cur = next;
                }
            }
        }

        return cur;
    }


    public SingleTreeNode expand(int action) {
        if (children[action] != null) 
            return children[action];

        StateObservation nextState = state.copy();
        nextState.advance(Agent.actions[action]);

        SingleTreeNode tn = new SingleTreeNode(nextState, this, this.m_rnd);
        children[action] = tn;
        
        return tn;
    }

    public SingleTreeNode uct() {

        SingleTreeNode selected = null;
        double bestValue = -Double.MAX_VALUE;
        for (SingleTreeNode child : this.children)
        {
            if (child == null) continue;

            ParetoFront normSet = child.paretoSet.normalise(bounds);
            double childValue = normSet.computeHypervolume() / (child.nVisits + this.epsilon);
            double uctValue = childValue + SingleMCTSPlayer.K * Math.sqrt(Math.log(this.nVisits + 1) / 
                                                                          (child.nVisits + this.epsilon));

            uctValue = Utils.noise(uctValue, this.epsilon, this.m_rnd.nextDouble()); // break ties randomly
            //System.out.println("norm child value: " + childValue);

            if (uctValue > bestValue) {
                selected = child;
                bestValue = uctValue;
            }
        }
        if (selected == null) {
            throw new RuntimeException("Warning! returning null: " + bestValue + " : " + this.children.length + " "
                    + bounds[0] + " " + bounds[1]);
        }

        return selected;
    }


    public Solution rollOut()
    {
        StateObservation rollerState = state.copy();
        int thisDepth = this.m_depth;

        while (!finishRollout(rollerState, thisDepth)) {

            int action = m_rnd.nextInt(Agent.num_actions);
            rollerState.advance(Agent.actions[action]);
            thisDepth++;
        }

        Solution rewards = rewards(rollerState);

        // update bounds for normalisation
        updateBounds(this, rewards);

        return rewards;
    }

    public void updateBounds(SingleTreeNode node, Solution rewards) {
        for (int idx = 0; idx < Agent.NUM_OBJECTIVES; idx++) {
            double reward = rewards.get(idx);
            if (reward < bounds[idx][0])
                bounds[idx][0] = reward;
            if (reward > bounds[idx][1])
                bounds[idx][1] = reward;
        }
    }

    public Solution rewards(StateObservation state) {
        Solution list = new Solution(Agent.NUM_OBJECTIVES);
        for (int idx = 0; idx < Agent.NUM_OBJECTIVES; idx++) {
            double value = Agent.objectives[idx].heuristic.value(state);
            list.set(idx, value);
        }
        return list;
    }

    public boolean finishRollout(StateObservation rollerState, int depth)
    {
        if(depth >= SingleMCTSPlayer.ROLLOUT_DEPTH)      //rollout end condition.
            return true;

        if(rollerState.isGameOver())               //end of game
            return true;

        return false;
    }

    public void backUp(SingleTreeNode node, Solution rewardVector)
    {
        boolean dominated = false;
        SingleTreeNode n = node;
        while (n != null) {
            n.nVisits++;
            for (int i = 0; i < Agent.NUM_OBJECTIVES; i++)
                n.accumulatedRewardVector[i] = n.accumulatedRewardVector[i] + rewardVector.get(i);

            if (!dominated) {
                if (n.paretoSet.dominates(rewardVector))
                    dominated = true;
                else
                    n.paretoSet.add(rewardVector);
            }
            
            // update bounds
            updateBounds(n, rewardVector);
            
            n.optionsHelper.optionFinished();

            n = n.parent;
        }
    }

    public Pair<Integer, Double> recommendationPolicy() {
        // 1. What is the best possible result?
        ParetoFront normPareto = paretoSet.normalise(bounds);
        // System.out.println(paretoSet);
        Pair<Integer, Double> bestSolutionWithScore = normPareto.findBestSolution();
        int bestSolutionIdx = bestSolutionWithScore.getKey();
        double result = bestSolutionWithScore.getValue();
        Solution bestSolution = paretoSet.getSolution(bestSolutionIdx);

        // 2. Which action leads to this result?
        List<Integer> bestActions = new ArrayList<Integer>(0);
        for (int idx = 0; idx < children.length; idx++)
            if (children[idx] != null)
                if (children[idx].paretoSet.contains(bestSolution))
                    bestActions.add(idx);

        // 3. Pick action
        if (bestActions.size() == 0) {
            System.out.println("bestActions.size() == 0 ?");    // all children are worse than their parent...
            return new Pair<>(m_rnd.nextInt(Agent.num_actions), result);
        } else if (bestActions.size() == 1) 
            return new Pair<>(bestActions.get(0), result);      // only one action leads to optimum, nice!
        else if (bestActions.size() == Agent.num_actions) {
            // System.out.println("All are possible");
            int n = m_rnd.nextInt(bestActions.size());          // sparse rewards, all actions same
            return new Pair<>(bestActions.get(n), result);
        }
        else if (bestActions.size() > 1 && bestActions.size() < Agent.num_actions) {
            int n = m_rnd.nextInt(bestActions.size());          // multiple roads to Rome, pick random (?)
            return new Pair<>(bestActions.get(n), result);
        }
        else
            throw new Error("More best actions than there are actions");
    }

    private double value() {

        double score = 0;
        int denom = 0;
        for (int objIdx = 0; objIdx < Agent.NUM_OBJECTIVES; objIdx++) {
            score += accumulatedRewardVector[objIdx] * Agent.objectives[objIdx].weight;
            denom += Agent.objectives[objIdx].weight;
        }
        score /= denom;
        return score;
    }

    public Pair<Integer, Double> bestAction()
    {
        int selected = -1;
        double selectedValue = -Double.MAX_VALUE;
        double bestValue = -Double.MAX_VALUE;

        for (int i=0; i<children.length; i++) {

            if(children[i] == null) 
                continue;

            //double tieBreaker = m_rnd.nextDouble() * epsilon;
            double childValue = children[i].value() / (children[i].nVisits + this.epsilon);
            childValue = Utils.noise(childValue, this.epsilon, this.m_rnd.nextDouble());     //break ties randomly
            if (childValue > bestValue) {
                bestValue = childValue;
                selectedValue = children[i].value() / children[i].nVisits;
                selected = i;
            }
        }

        if (selected == -1)
        {
            System.out.println("Unexpected selection!");
            selected = 0;
        }

        return new Pair<>(selected, selectedValue);
    }

    public boolean notFullyExpanded() {
        for (SingleTreeNode tn : children) {
            if (tn == null) {
                return true;
            }
        }

        return false;
    }

    public Introspection introspect() {
        return introspection;
    }
}
