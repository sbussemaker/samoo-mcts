package tracks.singlePlayer.stefan.samooMCTS.options;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import core.game.Observation;
import core.game.StateObservation;
import ontology.Types;
import tools.Vector2d;

import tracks.singlePlayer.stefan.samooMCTS.Agent;

public class AvoidNearestNpcOption extends Option {
    private Random rnd = new Random();

    public AvoidNearestNpcOption() {
        // has no invocation arguments
        // has no subtypes
    }

    @Override
    public int hashCode() {
        return 50;
    }

    /** any state s_t that has an NPC on the observation grid */
    public boolean initiates(StateObservation so) {
        return true;
    }
    
    /** any state s_t + 1 */
    public boolean terminates(StateObservation so) {
        return true;
    }

    /** apply action that moves away from the NPC */
    public int getAction(StateObservation so) {
        Observation nearestNPC = nearestNPC(so);

        Vector2d npcPos = nearestNPC.position;
        Vector2d myPos = nearestNPC.reference;

        // 9 situations: 4 straight lines, 4 quadrants, same position
        // origin is upper left, so higher x is right and higher y is down
        if (myPos.x == npcPos.x && myPos.y == npcPos.y)
            return 0;
        else if (myPos.x == npcPos.x && myPos.y > npcPos.y)
            return otherActionThan(new String[]{ "ACTION_UP" });
        else if (myPos.x == npcPos.x && myPos.y < npcPos.y)
            return otherActionThan(new String[]{ "ACTION_DOWN" });
        else if (myPos.x > npcPos.x && myPos.y == npcPos.y)
            return otherActionThan(new String[]{ "ACTION_LEFT" });
        else if (myPos.x < npcPos.x && myPos.y == npcPos.y)
            return otherActionThan(new String[]{ "ACTION_RIGHT" });
        else if (myPos.x > npcPos.x && myPos.y > npcPos.y)
            return otherActionThan(new String[]{ "ACTION_UP", "ACTION_LEFT" });
        else if (myPos.x > npcPos.x && myPos.y < npcPos.y)
            return otherActionThan(new String[]{ "ACTION_DOWN", "ACTION_LEFT" });
        else if (myPos.x < npcPos.x && myPos.y > npcPos.y)
            return otherActionThan(new String[]{ "ACTION_UP", "ACTION_RIGHT" });
        else if (myPos.x < npcPos.x && myPos.y < npcPos.y)
            return otherActionThan(new String[]{ "ACTION_DOWN", "ACTION_RIGHT" });

        throw new Error("Something went wrong");
    }

    /** Selects an action which does NOT move in the specified directions */
    private int otherActionThan(String[] actionNames) {
        ArrayList<Types.ACTIONS> possibleDirections = new ArrayList<Types.ACTIONS>();
        for (Types.ACTIONS action: Agent.actions)
            if (action.name() != "ACTION_USE")
                possibleDirections.add(action);

        ArrayList<String> avoid = new ArrayList<>();
        Collections.addAll(avoid, actionNames);
        
        ArrayList<Types.ACTIONS> eligableDirections = new ArrayList<Types.ACTIONS>();
        for (Types.ACTIONS action: possibleDirections)
            if (!avoid.contains(action.name()))
                eligableDirections.add(action);

        int numEligable = eligableDirections.size();
        int random = rnd.nextInt(numEligable);
        Types.ACTIONS chosen = eligableDirections.get(random);

        for (int idx = 0; idx < Agent.actions.length; idx++)
            if (Agent.actions[idx] == chosen)
                return idx;

        throw new Error("Something went horribly wrong");
    }

    private Observation nearestNPC(StateObservation so) {
        Vector2d avatarPosition = so.getAvatarPosition();
        ArrayList<Observation>[] npcs = so.getNPCPositions(avatarPosition);

        Observation nearestNPC = null;
        double minDistance = Double.MAX_VALUE;
        for (ArrayList<Observation> obsType: npcs) {
            if (obsType.size() == 0)
                continue;

            Observation nearest = obsType.get(0);
            if (nearest.sqDist < minDistance) {
                minDistance = nearest.sqDist;
                nearestNPC = nearest;
            }
        }

        if (nearestNPC == null)
            throw new Error("no nearest neighbor found");

        return nearestNPC;
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject)
            return true;

        if (!(otherObject instanceof ActionOption))
            return false;

        return true;    // can only be one nearest npc rule active
    }
}
