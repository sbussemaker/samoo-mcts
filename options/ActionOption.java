package tracks.singlePlayer.stefan.samooMCTS.options;

import core.game.StateObservation;

public class ActionOption extends Option {
    private int action;

    /** is invoked with an action */
    public ActionOption(int action) {
        this.action = action;
    }

    @Override
    public int hashCode() {
        // to be able to compare two instances
        return 40 + action;
    }

    /** any state s_t */
    public boolean initiates(StateObservation so) {
        return true;
    }
    
    /** any state s_t + 1 */
    public boolean terminates(StateObservation so) {
        return true;
    }

    /** the action corresponding to the subtype */
    public int getAction(StateObservation so) {
        return action;
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject)
            return true;

        if (!(otherObject instanceof ActionOption))
            return false;

        ActionOption other = (ActionOption) otherObject;
        return this.action == other.action;
    }
}
