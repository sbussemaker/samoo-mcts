package tracks.singlePlayer.stefan.samooMCTS.options;

import java.util.ArrayList;

import core.game.Observation;
import core.game.StateObservation;
import ontology.Types;
import ontology.Types.ACTIONS;
import tools.Vector2d;
import tools.pathfinder.Node;
import tracks.singlePlayer.stefan.samooMCTS.Agent;
import tracks.singlePlayer.stefan.samooMCTS.SingleMCTSPlayer;

public class GoToMovableOption extends Option {
    private Observation goal;

    @Override
    public int hashCode() {
        return 2000 + goal.obsID;
    }

    public GoToMovableOption(Observation goal) {
        this.goal = goal;
    }

    /** Any state s_t with the goal sprite in the observation grid */
    public boolean initiates(StateObservation so) {
        boolean goalPresent = stateContainsGoal(so, goal);
        boolean goalReachable = canReach(so, goal);
        boolean goalElsewhere = !equals(so, so.getAvatarPosition(), new Vector2d(goal.position));

        return goalPresent && goalReachable && goalElsewhere;
    }

    /** See if goal is still present in this state */
    private boolean stateContainsGoal(StateObservation so, Observation goal) {
        ArrayList<Observation>[] movablePositionss = so.getMovablePositions();
        if (movablePositionss != null)
            for (ArrayList<Observation> movablePositions : movablePositionss)
                for (Observation obs : movablePositions)
                    if (obs.obsID == goal.obsID)
                        return true;

        return false;
    }

    /**
     * Check if Agent can reach the goal. Goal is reachable if Agent can find path to goal and goal can be reached in
     * ROLLOUT_DEPTH steps
     */
    private boolean canReach(StateObservation so, Observation goal) {
        ArrayList<Node> path = Agent.getPathTo(so, goal);
        
        boolean reachable = Agent.getPathTo(so, goal) != null;
        if (!reachable)
            return false;

        boolean near = path.size() <= SingleMCTSPlayer.ROLLOUT_DEPTH;
        if (!near)
            return false;

        return true;
    }

    /** 
     * Any state s_t+n in which the goal sprite location is the same as the avatar location and all the states s_t+n 
     * that do not contain the goal sprite 
     */
    public boolean terminates(StateObservation so) {
        if (!stateContainsGoal(so, goal))
            return true;

        Vector2d avatarPosition = so.getAvatarPosition();
        goal = getGoalFromState(so, goal);
        Vector2d pos = new Vector2d(goal.position);

        if (!canReach(so, goal))                // stop following goal if it can no longer be reached
            return true;

        return equals(so, avatarPosition, pos);
    }

    /** Update goal position from this new state */
    private Observation getGoalFromState(StateObservation so, Observation goal) {
        ArrayList<Observation>[] movablePositionss = so.getMovablePositions();
        if (movablePositionss != null)
            for (ArrayList<Observation> movablePositions : movablePositionss)
                for (Observation obs : movablePositions)
                    if (obs.obsID == goal.obsID)
                        return obs;

        throw new Error("Goal not found in movablepositions, but still requested it");
    }

    private boolean equals(StateObservation so, Vector2d start, Vector2d end) {
        int blockSize = so.getBlockSize();
        start.x = Math.round(start.x / blockSize);
        start.y = Math.round(start.y / blockSize);
        end.x = Math.round(end.x / blockSize);
        end.y = Math.round(end.y / blockSize);
        return start.equals(end);
    }

    /** Apply the action that leads to the goal sprite */
    public int getAction(StateObservation so) {
        ArrayList<Node> path = Agent.getPathTo(so, goal);

        if (path == null) {
            System.out.println("[GoToMovableOption]: No path found");
            return 0;
        }

        Node to = path.get(0);

        String action = "ACTION_NIL";
        if (to.comingFrom.x < 0)
            action = "ACTION_LEFT";
        else if (to.comingFrom.x > 0)
            action = "ACTION_RIGHT";
        else if (to.comingFrom.y > 0)
            action = "ACTION_DOWN";
        else if (to.comingFrom.y < 0)
            action = "ACTION_UP";
        
        Types.ACTIONS chosenAction = ACTIONS.fromString(action);
        for (int idx = 0; idx < Agent.actions.length; idx++)
            if (Agent.actions[idx] == chosenAction)
                return idx;

        throw new Error("Unique error message");
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject)
            return true;

        if (!(otherObject instanceof GoToMovableOption))
            return false;

        GoToMovableOption other = (GoToMovableOption) otherObject;
        return this.goal.equals(other.goal);
    }
}