package tracks.singlePlayer.stefan.samooMCTS.options;

import java.util.ArrayList;

import core.game.Observation;
import core.game.StateObservation;

public class WaitAndShootOption extends Option {

    @Override
    public int hashCode() {
        return 60;
    }

    public boolean initiates(StateObservation so) {
        return true;
    }

    public boolean terminates(StateObservation so) {
        ArrayList<Observation>[] avatarSpritesPositionss = so.getFromAvatarSpritesPositions();
        if (avatarSpritesPositionss == null)
            return true;


        if (avatarSpritesPositionss.length > 1)
            throw new Error("More than one fromAvatarSpritePositions?");

        return avatarSpritesPositionss[0].size() > 0;
    }

    public int getAction(StateObservation so) {
        return 0;
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject)
            return true;

        if (!(otherObject instanceof ActionOption))
            return false;

        return true;
    }
}
