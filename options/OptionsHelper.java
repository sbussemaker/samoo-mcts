package tracks.singlePlayer.stefan.samooMCTS.options;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import core.game.Observation;
import core.game.StateObservation;
import ontology.Types;
import tracks.singlePlayer.stefan.samooMCTS.options.Option;
import tracks.singlePlayer.stefan.samooMCTS.options.WaitAndShootOption;
import tracks.singlePlayer.stefan.samooMCTS.Agent;
import tracks.singlePlayer.stefan.samooMCTS.options.ActionOption;
import tracks.singlePlayer.stefan.samooMCTS.options.AvoidNearestNpcOption;
import tracks.singlePlayer.stefan.samooMCTS.options.GoNearMovableOption;
import tracks.singlePlayer.stefan.samooMCTS.options.GoToMovableOption;
import tracks.singlePlayer.stefan.samooMCTS.options.GoToNearestSpriteOfType;
import tracks.singlePlayer.stefan.samooMCTS.options.GoToPositionOption;

public class OptionsHelper {
    public Option currentOption;

    private Random rnd = new Random();
    private StateObservation so;

    public Set<Option> options = new HashSet<Option>();
    public Set<Option> finishedOptions = new HashSet<Option>();

    // Invocations
    public OptionsHelper(StateObservation so, int num_actions) {
        this.so = so;

        //
        // ActionOption
        //
        for (int num_action = 0; num_action < num_actions; num_action++)
            options.add(new ActionOption(num_action));  // one subtype is created for each action in action set A

        if (!Agent.ENABLE_OPTIONS)      // only allowing the actions as options equals not having options at all
            return;

        //
        // AvoidNearestNPCOption
        //
        if (containsNPC(so))
            options.add(new AvoidNearestNpcOption());       // has no invocation arguments

        //
        // GoNear- / GoToMovableOption
        //
        ArrayList<Observation>[] movablePositionss = so.getMovablePositions(so.getAvatarPosition());
        if (movablePositionss != null)
            for (ArrayList<Observation> movablePositions : movablePositionss)
                for (Observation movablePosition : movablePositions) {
                    // options.add(new GoNearMovableOption(movablePosition));  // invokes on a movable sprite in the observation grid
                    options.add(new GoToMovableOption(movablePosition));    // invokes on a movable sprite in the observation grid
                }        

        //
        // GoToNearestSpriteOfType
        //
        ArrayList<Observation>[] resourcePositionss = so.getResourcesPositions(so.getAvatarPosition());
        if (resourcePositionss != null)
            for (ArrayList<Observation> resourcePositions : resourcePositionss)
                if (resourcePositions.size() > 0)
                    options.add(new GoToNearestSpriteOfType(resourcePositions.get(0)));       // invokes a sprite type

        // ArrayList<Observation>[] movablePositionss = so.getMovablePositions(so.getAvatarPosition());
        // if (movablePositionss != null)
        //     for (ArrayList<Observation> movablePositions : movablePositionss)
        //         if (movablePositions.size() > 0)
        //             options.add(new GoToNearestSpriteOfType(movablePositions.get(0)));    // invokes on a movable sprite in the observation grid

        ArrayList<Observation>[] immovablePositionss = so.getImmovablePositions();
        immovablePositionss = (immovablePositionss != null) ? Arrays.copyOfRange(immovablePositionss, 1, immovablePositionss.length) : null;
        if (immovablePositionss != null)
            for (ArrayList<Observation> immovablePositions : immovablePositionss)
                if (immovablePositions.size() > 0)
                    options.add(new GoToNearestSpriteOfType(immovablePositions.get(0)));
                // for (Observation immovablePosition : immovablePositions)
                //     options.add(new GoToPositionOption(immovablePosition));

        //
        // GoToPositionOption
        //
        // invokes a specific goal position in the grid or with a static sprite
        ArrayList<Observation>[] portalsPositionss = so.getPortalsPositions();
        if (portalsPositionss != null)
            for (ArrayList<Observation> portalsPositions : portalsPositionss)
                for (Observation portalsPosition : portalsPositions)
                    options.add(new GoToPositionOption(portalsPosition));

        ArrayList<Observation>[] NPCpositionss = so.getNPCPositions();
        if (NPCpositionss != null)
            for (ArrayList<Observation> NPCpositions : NPCpositionss)
                for (Observation npc : NPCpositions) 
                    options.add(new GoToPositionOption(npc));

        //
        // WaitAndShootOption
        //
        for (Types.ACTIONS action: Agent.actions)
            if (action.name() == "ACTION_USE")
                options.add(new WaitAndShootOption());

        if (options.size() == 0)
            throw new Error("There are no options to choose from");
    }

    /** returns options which invocation contains state */
    public Option[] availableOptions() {
        Set<Option> eligableOptions = new HashSet<>();
        for (Option option: options) {
            if (option.initiates(so))
                eligableOptions.add(option);
        }

        return eligableOptions.toArray(new Option[0]);
    }

    public boolean optionStops(StateObservation so) {
        return currentOption.terminates(so);
    }

    public Option[] expandedOptions() {
        return finishedOptions.toArray(new Option[0]);
    }

    public boolean equals(Option[] a, Option[] b) {
        // Changed to check if a contains elements not present in b

        Set<Option> listA = new HashSet<>(Arrays.asList(a));
        Set<Option> listB = new HashSet<>(Arrays.asList(b));
        // boolean same = listA.equals(listB);
        boolean same = listB.containsAll(listA);

        return same;
    }

    public Option randomElement(Option[] p_s, Option[] m) {
        Set<Option> listA = new HashSet<>(Arrays.asList(p_s));
        Set<Option> listB = new HashSet<>(Arrays.asList(m));
        listA.removeAll(listB);


        Option[] eligable = listA.toArray(new Option[0]);
        if (eligable.length == 0)
            System.out.println("This is not supposed to happen");
        int idx = rnd.nextInt(eligable.length);
        return eligable[idx];
    }

    public int getAction(Option w, StateObservation so) {
        return w.getAction(so);
    }

    public void optionFinished() {
        if (currentOption == null)
            return;

        finishedOptions.add(currentOption);
        currentOption = null;
    }

    public boolean containsNPC(StateObservation so) {
        ArrayList<Observation>[] npcPositions = so.getNPCPositions();
        return (npcPositions != null && npcPositions.length > 0 && npcPositions[0].size() > 0);
    }
}
