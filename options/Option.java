package tracks.singlePlayer.stefan.samooMCTS.options;

import core.game.StateObservation;

public abstract class Option {

    /** Initiation set */
    public abstract boolean initiates(StateObservation so);

    /** Termination set */
    public abstract boolean terminates(StateObservation so);

    /** Policy */
    public abstract int getAction(StateObservation so);

    @Override
    public abstract boolean equals(Object other);
}
