package tracks.singlePlayer.stefan.samooMCTS.pareto;

public class Solution {
    private double[] solution;

    public Solution(int numObjectives) {
        solution = new double[numObjectives];
    }

    public double get(int idx) {
        return solution[idx];
    }

    public void set(int idx, double value) {
        solution[idx] = value;
    }

    /**
     * It is said that a solution x dominates another solution y. Dominates gaat dus
     * over 2 solutions. Als x gedominate wordt door geen enkele y, voeg to aan de
     * set
     */
    public boolean dominates(Solution other) {
        // 1.fi(x) not worse than fi(y) for all i
        boolean notWorse = true;
        for (int idx = 0; idx < solution.length; idx++)
            if (get(idx) < other.get(idx))
                notWorse = false;

        // 2. For at least one objective j: fj(x) is better than its analogous
        // conterpart in fj(y)
        boolean oneBetter = false;
        for (int idx = 0; idx < solution.length; idx++)
            if (get(idx) > other.get(idx))
                oneBetter = true;

        return notWorse && oneBetter;
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject){
            return true;
        }

        if (!(otherObject instanceof Solution)){
            return false;
        }

        Solution other = (Solution) otherObject;
        boolean equals = true;
        for (int objIdx = 0; objIdx < solution.length; objIdx++) {
            if (solution[objIdx] != other.solution[objIdx]) {
                equals = false;
                break;
            }
        }
        return equals;
    }

    @Override
    public String toString() {
        String out = "[";
        for (int numObjective = 0; numObjective < solution.length; numObjective++) {
            if (out.length() > 1)
                out += ", ";

            out += solution[numObjective];
        }
        out += "]";

        return out;
    }
}
