package tracks.singlePlayer.stefan.samooMCTS.pareto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tools.Pair;
import tools.Utils;
import tracks.singlePlayer.stefan.samooMCTS.Agent;

/**
 * Pareto set, forms local Pareto front a.k.a. non-dominated set. Contains
 * values, computes hypervolume indicator
 */
public class ParetoFront {
    private List<Solution> front = new ArrayList<Solution>(0);

    /**
     * 
     */
    public void add(Solution rewardVector) {
        if (!front.contains(rewardVector))
            front.add(rewardVector);

        // Find dominated solutions, and remove
        List<Solution> delet = new ArrayList<Solution>(0);
        for (Solution solution : front)
            if (rewardVector.dominates(solution))
                delet.add((solution));

        front.removeAll(delet);
        front.removeAll(Collections.singleton(null));
    }

    public boolean contains(Solution item) {
        boolean contains = false;
        for (Solution solution : front) {
            if (solution.equals(item)) {
                contains = true;
                break;
            }
        }
        return contains;
    }

    /**
     * Does front dominate
     */
    public boolean dominates(Solution rewardVector) {
        boolean dominated = false;
        for (Solution solution : front)
            if (solution.dominates(rewardVector)) {
                dominated = true;
                break;
            }

        return dominated;
    }

    public ParetoFront normalise(double[][] bounds) {
        ParetoFront newSet = new ParetoFront();

        for (Solution solution : front) {
            Solution normSolution = new Solution(Agent.NUM_OBJECTIVES);
            for (int objIdx = 0; objIdx < Agent.NUM_OBJECTIVES; objIdx++) {
                normSolution.set(objIdx, Utils.normalise(solution.get(objIdx), bounds[objIdx][0], bounds[objIdx][1]));
            }
            newSet.add(normSolution);
        }

        return newSet;
    }

    public void log() {
        for (Solution solution : front)
            System.out.println(solution);
        System.out.print('\n');
    }

    public Solution getSolution(int idx) {
        return front.get(idx);
    }

    /** scalarization */
    public Pair<Integer, Double> findBestSolution() {
        double bestValue = -Double.MAX_VALUE;
        int bestValueIdx = -1;
        for (int idx = 0; idx < front.size(); ++idx) {
            Solution solution = front.get(idx);
            double fitness = 0;
            int denom = 0;
            for (int objIdx = 0; objIdx < Agent.NUM_OBJECTIVES; objIdx++) {
                fitness += solution.get(objIdx) * Agent.objectives[objIdx].weight;
                denom += Agent.objectives[objIdx].weight;
            }
            fitness /= denom;

            if (fitness > bestValue) {
                bestValue = fitness;
                bestValueIdx = idx;
            }
        }

        if (bestValue == -Double.MAX_VALUE || bestValueIdx == -1)
            throw new Error("No best value found!");

        return new Pair<>(bestValueIdx, bestValue);
    }

    /**
     * Hypervolume Indicator (HV) Measure the quality of a non-dominated set by
     * looking at both the diversity and convergence. The higher the value of HV(P),
     * the better the front
     */
    public double computeHypervolume() {
        if (front.size() == 0) {
            return 0;
        }
    
        double answer = calculateHypervolume(front, front.size(), Agent.NUM_OBJECTIVES);
        return answer;
    }

    // https://github.com/MOEAFramework/MOEAFramework/blob/master/src/org/moeaframework/core/Solution.java
    private double calculateHypervolume(List<Solution> population, int numberOfSolutions,
            int numberOfObjectives) {
        double volume = 0.0;
        double distance = 0.0;
        int n = numberOfSolutions;

        while (n > 0) {
            double tempVolume = population.get(0).get(0);
            double tempDistance = surfaceUnchangedTo(population, n, numberOfObjectives - 1);
            volume += tempVolume * (tempDistance - distance);
            distance = tempDistance;
            n = reduceNondominatedSet(population, n, numberOfObjectives - 1, distance);
        }

        return volume;
    }

    private double surfaceUnchangedTo(List<Solution> population, int numberOfSolutions, int objective) {
        double min = population.get(0).get(objective);

        for (int i = 1; i < numberOfSolutions; i++) {
            min = Math.min(min, population.get(i).get(objective));
        }

        return min;
    }

    private int reduceNondominatedSet(List<Solution> population, int numberOfSolutions, int objective,
            double threshold) {
        int n = numberOfSolutions;

        for (int i = 0; i < n; i++) {
            if (population.get(i).get(objective) <= threshold) {
                n--;
                swap(population, i, n);
            }
        }

        return n;
    }

    private void swap(List<Solution> population, int i, int j) {
        Solution temp = population.get(i);
        population.set(i, population.get(j));
        population.set(j, temp);
    }

    @Override
    public String toString() {
        String out = "Pareto set: \n";
        for (Solution solution : front)
            out = out + solution + '\n';
        return out;
    }
}
