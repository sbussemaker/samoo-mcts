package tracks.singlePlayer.stefan.samooMCTS;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import core.game.Observation;
import core.game.StateObservation;
import tools.Vector2d;
import tools.pathfinder.Node;
import tools.pathfinder.PathFinder;

public class PathPlanner {
    public static PathFinder pathf;
    private static int blockSize;

    private final int mapHeightPixels;
    private final int mapWidthPixels;
    private final int pixelsPerBlock;

    private ArrayList<Node> currentRoute;
    
    public PathPlanner(StateObservation so) {
        initPath(so);

        Dimension pixels = so.getWorldDimension();
        pixelsPerBlock = so.getBlockSize();
        mapHeightPixels = pixels.height;
        mapWidthPixels = pixels.width;
    }

    private void initPath(StateObservation so) {
        ArrayList<Integer> list = new ArrayList<>(0);
        list.add(0); //wall
        pathf = new PathFinder(list);
        pathf.run(so);
        blockSize = so.getBlockSize();
    }

    public ArrayList<Node> getPathTo(StateObservation so, Observation goal) {
        Vector2d start = new Vector2d(so.getAvatarPosition());
        Vector2d end = new Vector2d(goal.position);
        start.x = Math.round(start.x / blockSize);
        start.y = Math.round(start.y / blockSize);
        end.x = Math.round(end.x / blockSize);
        end.y = Math.round(end.y / blockSize);
        
        ArrayList<Node> route = pathf.getPath(start, end);
        currentRoute = route;
        
        return route;
    }

    // highlight cells with white dot
    public void draw(Graphics2D g) {
        BufferedImage imgBuf = new BufferedImage(mapWidthPixels, mapHeightPixels, BufferedImage.TYPE_INT_ARGB);
        Graphics2D img = imgBuf.createGraphics();

        for (Node n : currentRoute) {
            Color color = new Color(255, 255, 255, 100);
            img.setColor(color);
            img.fillRect((int) (n.position.x * pixelsPerBlock) + (int) (pixelsPerBlock / 4), 
                         (int) (n.position.y * pixelsPerBlock) + (int) (pixelsPerBlock / 4), 
                         (int) (pixelsPerBlock / 4), 
                         (int) (pixelsPerBlock / 4));
        }

        g.drawImage(imgBuf, 0, 0, mapWidthPixels, mapHeightPixels, null);
    }
}