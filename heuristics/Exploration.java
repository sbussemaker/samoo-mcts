package tracks.singlePlayer.stefan.samooMCTS.heuristics;

import core.game.StateObservation;
import tools.Vector2d;
import tracks.singlePlayer.stefan.samooMCTS.Agent;

/**
 * Exploration heuristic using pheromones. Encourages the agent to go to new areas
 */
public class Exploration extends AbstractHeuristic {

    public double value(StateObservation state) {
        Vector2d avatarPos = state.getAvatarPosition();
        double value = 1 - Agent.pheromones.getPheromoneStrength(avatarPos);
        return value;
    }
}
