package tracks.singlePlayer.stefan.samooMCTS.heuristics;

public class WeightedHeuristic {
    public AbstractHeuristic heuristic;
    public int weight;

    public WeightedHeuristic(AbstractHeuristic heuristic, int weight) {
        this.heuristic = heuristic;
        this.weight = weight;
    }
}