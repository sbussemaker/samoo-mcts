package tracks.singlePlayer.stefan.samooMCTS.heuristics;

import core.game.StateObservation;

public abstract class AbstractHeuristic {

    // Computes the state value for a certain heuristic.
    public abstract double value(StateObservation a_gameState);

}